package log

var (
	defaultOptions = &Options{
		Filename:   "./log", //日志文件，如果文件夹不存在会自动创建
		MaxSize:    100,     //文件大小限制,单位MB
		MaxBackups: 5,       //最大保留日志文件数量
		MaxAge:     30,      //日志文件保留天数
		Compress:   false,   //是否压缩处理
	}
)

// optionFunc 设置参数函数
type optionFunc func(*Options)

// Options Log初始化参数
type Options struct {
	Filename   string `mapstructure:"file_name"`   // 日志文件名
	MaxSize    int    `mapstructure:"max_size"`    // 文件大小限制,单位MB
	MaxBackups int    `mapstructure:"max_backups"` // 最大保留日志文件数量
	MaxAge     int    `mapstructure:"max_age"`     // 日志文件保留天数
	Compress   bool   `mapstructure:"compress"`    // 是否压缩处理
	Level      int8   `mapstructure:"level"`       // 日志级别
}

// Filename 设置日志文件名，不需要带文件后缀
func Filename(filename string) optionFunc {
	return func(opt *Options) {
		opt.Filename = filename
	}
}

// MaxSize 设置文件大小限制,单位MB
func MaxSize(maxSize int) optionFunc {
	return func(opt *Options) {
		opt.MaxSize = maxSize
	}
}

// MaxBackups 设置最大保留日志文件数量
func MaxBackups(maxBackups int) optionFunc {
	return func(opt *Options) {
		opt.MaxBackups = maxBackups
	}
}

// MaxAge 设置日志文件保留天数
func MaxAge(maxAge int) optionFunc {
	return func(opt *Options) {
		opt.MaxAge = maxAge
	}
}

// Compress 设置是否压缩处理
func Compress(compress bool) optionFunc {
	return func(opt *Options) {
		opt.Compress = compress
	}
}

// LogLevel 设置日志级别
func LogLevel(level int8) optionFunc {
	return func(opt *Options) {
		opt.Level = level
	}
}

// SetOptions 设置log选项
func SetOptions(opts ...optionFunc) {
	for _, opt := range opts {
		opt(defaultOptions)
	}
}
