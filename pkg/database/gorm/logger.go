package gorm

import (
	"context"
	"time"

	"gitee.com/zacyuan/yuan/pkg/log"
	"gorm.io/gorm/logger"
)

type GormLogger struct {
}

func (c *GormLogger) LogMode(logger.LogLevel) logger.Interface {
	return c
}

func (c *GormLogger) Info(ctx context.Context, format string, args ...interface{}) {
	log.FormContext(ctx).Infof(format, args...)
}

func (c *GormLogger) Warn(ctx context.Context, format string, args ...interface{}) {
	log.FormContext(ctx).Warnf(format, args...)
}

func (c *GormLogger) Error(ctx context.Context, format string, args ...interface{}) {
	log.FormContext(ctx).Errorf(format, args...)
}

func (c *GormLogger) Trace(ctx context.Context, begin time.Time, fc func() (string, int64), err error) {
	msg, count := fc()
	log.FormContext(ctx).Infof("[%v] [rows:%d] [%s]", time.Since(begin), count, msg)
}
