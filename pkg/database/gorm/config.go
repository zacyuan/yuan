package gorm

import (
	"runtime"

	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"gorm.io/gorm/schema"
)

type Config struct {
	GormConfig   *GormConfig
	Driver       string `mapstructure:"driver"`
	DataSource   string `mapstructure:"data_source"`
	MaxIdleConns int    `mapstructure:"max_idle_conns"`
	MaxOpenConns int    `mapstructure:"max_open_conns"`
	Startup      bool   `mapstructure:"startup"`
	WriteLog     bool   `mapstructure:"write_log"`
	Name         string `mapstructure:"mapstructure"`
	Interceptors []Hook
}

func DefaultConfig() *Config {
	return &Config{
		GormConfig: &gorm.Config{
			Logger: logger.Discard,
			NamingStrategy: schema.NamingStrategy{
				SingularTable: true,
			},
		},
		Driver:       "mysql",
		MaxIdleConns: runtime.GOMAXPROCS(0) * 8,
		MaxOpenConns: runtime.GOMAXPROCS(0) * 8,
		Startup:      true,
		WriteLog:     false,
	}
}
