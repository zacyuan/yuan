package gorm

import "gorm.io/gorm"

const (
	ScanInitialized         = gorm.ScanInitialized
	ScanUpdate              = gorm.ScanUpdate
	ScanOnConflictDoNothing = gorm.ScanOnConflictDoNothing
)

type (
	Association                   = gorm.Association
	GormConfig                    = gorm.Config
	Option                        = gorm.Option
	DB                            = gorm.DB
	Session                       = gorm.Session
	Dialector                     = gorm.Dialector
	Plugin                        = gorm.Plugin
	ConnPool                      = gorm.ConnPool
	SavePointerDialectorInterface = gorm.SavePointerDialectorInterface
	TxBeginner                    = gorm.TxBeginner
	ConnPoolBeginner              = gorm.ConnPoolBeginner
	TxCommitter                   = gorm.TxCommitter
	Tx                            = gorm.Tx
	Valuer                        = gorm.Valuer
	GetDBConnector                = gorm.GetDBConnector
	Rows                          = gorm.Rows
	ViewOption                    = gorm.ViewOption
	ColumnType                    = gorm.ColumnType
	Index                         = gorm.Index
	Migrator                      = gorm.Migrator
	Model                         = gorm.Model
	Stmt                          = gorm.Stmt
	ParamsFilter                  = gorm.ParamsFilter
	PreparedStmtDB                = gorm.PreparedStmtDB
	PreparedStmtTX                = gorm.PreparedStmtTX
	ScanMode                      = gorm.ScanMode
	DeletedAt                     = gorm.DeletedAt
	SoftDeleteQueryClause         = gorm.SoftDeleteQueryClause
	SoftDeleteUpdateClause        = gorm.SoftDeleteUpdateClause
	SoftDeleteDeleteClause        = gorm.SoftDeleteDeleteClause
	Statement                     = gorm.Statement
	StatementModifier             = gorm.StatementModifier
)

var (
	Expr = gorm.Expr
	Scan = gorm.Scan

	ErrRecordNotFound        = gorm.ErrRecordNotFound
	ErrInvalidTransaction    = gorm.ErrInvalidTransaction
	ErrNotImplemented        = gorm.ErrNotImplemented
	ErrMissingWhereClause    = gorm.ErrMissingWhereClause
	ErrUnsupportedRelation   = gorm.ErrUnsupportedRelation
	ErrPrimaryKeyRequired    = gorm.ErrPrimaryKeyRequired
	ErrModelValueRequired    = gorm.ErrModelValueRequired
	ErrInvalidData           = gorm.ErrInvalidData
	ErrUnsupportedDriver     = gorm.ErrUnsupportedDriver
	ErrRegistered            = gorm.ErrRegistered
	ErrInvalidField          = gorm.ErrInvalidField
	ErrEmptySlice            = gorm.ErrEmptySlice
	ErrDryRunModeUnsupported = gorm.ErrDryRunModeUnsupported
	ErrInvalidDB             = gorm.ErrInvalidDB
	ErrInvalidValue          = gorm.ErrInvalidValue
	ErrInvalidValueOfLength  = gorm.ErrInvalidValueOfLength
	ErrPreloadNotAllowed     = gorm.ErrPreloadNotAllowed
)
