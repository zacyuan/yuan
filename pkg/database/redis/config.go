package redis

type Config struct {
	Addr     string `mapstructure:"addr"`
	Password string `mapstructure:"password"`
	Startup  bool   `mapstructure:"startup"`
	WriteLog bool   `mapstructure:"write_log"`
	Name     string `mapstructure:"mapstructure"`
	Hooks    []Hook
}

func DefaultConfig() *Config {
	return &Config{
		Startup:  true,
		WriteLog: false,
	}
}
