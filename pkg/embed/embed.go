package cembed

import (
	"embed"
	"io/fs"
	"path"
)

type InnerFS struct {
	// embed静态资源
	fs embed.FS
	// 设置embed文件到静态资源的相对路径，也就是embed注释里的路径
	path string
}

// NewInnerFS 创建EmbedFS对象
func NewInnerFS(fs embed.FS, path string) *InnerFS {
	return &InnerFS{
		fs:   fs,
		path: path,
	}
}

// Open 打开文件
func (c *InnerFS) Open(name string) (fs.File, error) {
	fullName := path.Join(c.path, name)
	return c.fs.Open(fullName)
}

// ReadDir 读取目录
func (c *InnerFS) ReadDir(name string) ([]fs.DirEntry, error) {
	fullName := path.Join(c.path, name)
	return c.fs.ReadDir(fullName)
}

// ReadFile 读取文件
func (c *InnerFS) ReadFile(name string) ([]byte, error) {
	fullName := path.Join(c.path, name)
	return c.fs.ReadFile(fullName)
}
