package config

import (
	"github.com/spf13/viper"
)

var (
	config = viper.New()
	files  = []string{"conf"}
	paths  = []string{"./"}

	Get                     = config.Get
	GetBool                 = config.GetBool
	GetFloat64              = config.GetFloat64
	GetInt                  = config.GetInt
	GetInt32                = config.GetInt32
	GetInt64                = config.GetInt64
	GetIntSlice             = config.GetIntSlice
	GetString               = config.GetString
	GetStringMap            = config.GetStringMap
	GetStringMapString      = config.GetStringMapString
	GetStringMapStringSlice = config.GetStringMapStringSlice
	GetStringSlice          = config.GetStringSlice
	GetUint                 = config.GetUint
	GetUint16               = config.GetUint16
	GetUint32               = config.GetUint32
	GetUint64               = config.GetUint64
	Set                     = config.Set
	Unmarshal               = config.Unmarshal
	UnmarshalKey            = config.UnmarshalKey
)

func AddConfigFiles(fs []string) {
	files = append(files, fs...)
}

func AddConfigPaths(ps []string) {
	paths = append(paths, ps...)
}

func Start() error {
	config.AutomaticEnv()
	_ = config.ReadInConfig()

	for _, p := range paths {
		config.AddConfigPath(p)
	}

	for _, f := range files {
		config.SetConfigName(f)
		_ = config.ReadInConfig()
	}
	return nil
}
