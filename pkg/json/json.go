package json

import (
	"encoding/json"

	"github.com/bytedance/sonic"
)

var (
	Compact    = json.Compact
	Indent     = json.Indent
	HTMLEscape = json.HTMLEscape

	Marshal         = sonic.Marshal
	MarshalString   = sonic.MarshalString
	Unmarshal       = sonic.Unmarshal
	UnmarshalString = sonic.UnmarshalString
	Get             = sonic.Get
	GetFromString   = sonic.GetFromString
	MarshalIndent   = sonic.ConfigDefault.MarshalIndent
	Pretouch        = sonic.Pretouch
)
