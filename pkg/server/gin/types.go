package gin

import "github.com/gin-gonic/gin"

type (
	Engine  = gin.Engine
	IRoutes = gin.IRoutes
	H       = gin.H
)
