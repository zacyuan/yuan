package mutex

import (
	"context"
	"time"
)

var (
	defaultTimeout        = 30 * time.Second // 默认超时时间
	defaultTTL            = 30 * time.Second // 默认存活时间
	defaultAutoRefreshTTL = false            // 默认自动刷新锁的存活时间
)

// Options 分布式锁参数
type Options struct {
	TTL            time.Duration // 锁的存活时间
	Timeout        time.Duration // 上锁超时时间
	AutoRefreshTTL bool          // 是否自动刷新锁的存活时间
	Retry          RetryStrategy // 重试策略
	Ctx            context.Context
}

// Option 设置分布锁参数
type Option func(*Options)

// newOptions 创建分布式锁参数对象
func newOptions(opts ...Option) *Options {
	opt := &Options{
		Timeout:        defaultTimeout,
		TTL:            defaultTTL,
		AutoRefreshTTL: defaultAutoRefreshTTL,
		Retry:          &defaultRetryStrategy{},
		Ctx:            context.Background(),
	}

	for _, one := range opts {
		one(opt)
	}
	return opt
}

// TTL 设置分布式的存活时间
func TTL(ttl time.Duration) Option {
	return func(opt *Options) {
		opt.TTL = ttl
	}
}

// Timeout 设置分布式的超时时间
func Timeout(timeout time.Duration) Option {
	return func(opt *Options) {
		opt.Timeout = timeout
	}
}

// AutoRefresh 设置分布式锁过期时间是否自动刷新
func AutoRefresh(autoRefresh bool) Option {
	return func(opt *Options) {
		opt.AutoRefreshTTL = autoRefresh
	}
}

// Context 设置上下文
func Context(ctx context.Context) Option {
	return func(opt *Options) {
		opt.Ctx = ctx
	}
}
