package http

import (
	"io"
	"net/url"
)

var (
	DefaultClient = NewClient()
)

func CloseIdleConnections() {
	DefaultClient.CloseIdleConnections()
}

func Get(url string) (*Response, error) {
	return DefaultClient.Get(url)
}

func Post(url, contentType string, body io.Reader) (resp *Response, err error) {
	return DefaultClient.Post(url, contentType, body)
}

func HTTP(method, url string, data []byte, headers map[string]string, cookies map[string]string) (*Response, error) {
	return DefaultClient.HTTP(method, url, data, headers, cookies)
}

func HTTPByte(method, url string, data []byte, headers map[string]string, cookies map[string]string) ([]byte, error) {
	return DefaultClient.HTTPByte(method, url, data, headers, cookies)
}

func GetByte(url string) ([]byte, error) {
	return DefaultClient.GetByte(url)
}

func PostByte(url string, data []byte, headers map[string]string, cookies map[string]string) ([]byte, error) {
	return DefaultClient.PostByte(url, data, headers, cookies)
}

func PostJSONByte(url string, data any, headers map[string]string, cookies map[string]string) ([]byte, error) {
	return DefaultClient.PostJSONByte(url, data, headers, cookies)
}

func PostFormByte(url string, data url.Values, headers map[string]string, cookies map[string]string) ([]byte, error) {
	return DefaultClient.PostFormByte(url, data, headers, cookies)
}
