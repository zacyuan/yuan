package http

import "net/http"

const (
	MethodGet     = http.MethodGet
	MethodHead    = http.MethodHead
	MethodPost    = http.MethodPost
	MethodPut     = http.MethodPut
	MethodPatch   = http.MethodPatch
	MethodDelete  = http.MethodDelete
	MethodConnect = http.MethodConnect
	MethodOptions = http.MethodOptions
	MethodTrace   = http.MethodTrace

	StatusOK                  = http.StatusOK
	StatusNotFound            = http.StatusNotFound
	StatusInternalServerError = http.StatusInternalServerError
)

type (
	Request   = http.Request
	Response  = http.Response
	Transport = http.Transport
	Cookie    = http.Cookie
)

var (
	NewRequest           = http.NewRequest
	ProxyFromEnvironment = http.ProxyFromEnvironment
)
