package main

import "github.com/spf13/cobra"

var rootCmd = &cobra.Command{
	Use:   "yuan",
	Short: "Yuan工具包。",
	Long:  ``,
	CompletionOptions: cobra.CompletionOptions{
		DisableDefaultCmd: true,
	},
}

func main() {
	cobra.CheckErr(rootCmd.Execute())
}
