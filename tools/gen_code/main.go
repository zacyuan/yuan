package main

import "github.com/spf13/cobra"

var rootCmd = &cobra.Command{
	Use:   "gen_code",
	Short: "代码生成工具",
	Long:  ``,
	CompletionOptions: cobra.CompletionOptions{
		DisableDefaultCmd: true,
	},
}

func main() {
	cobra.CheckErr(rootCmd.Execute())
}
